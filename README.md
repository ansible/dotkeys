# GPG keys, keyrings, and password store

This role copies the gnupg directory, the keyrings directory from the workstation
(that the user specified in `profiles_host`) and initialises and clones the password-store.

The `profiles_host` can be another already-provisioned workstation or
the Ansible controller itself, but should not point to a not-yet-provisioned
workstation.

With this design, this role **requires at least one existing provisioned workstation**,
and it means we can avoid storing duplicates (encrypted or not) of these sensitive
files just for the sake of the playbook.

## Issue: how do I change the duration gpg-key stays unlocked?

With increased use of passwordstore lookups in my playbooks,
I foresee a need to increase the time duration my gpg key stays unlocked
after decryption.

I think it's currently 600 seconds (10 minutes), but I cannot find where
this value is set. I no longer have a `~/.gnupg/gpg-agent.conf` file,
and I don't really understand how come.

```
taha@asks2:~
$ dpkg -l | grep pinentry
ii  pinentry-curses   1.1.0-1   amd64   curses-based PIN or pass-phrase entry dialog for GnuPG
ii  pinentry-gnome3   1.1.0-1   amd64   GNOME 3 PIN or pass-phrase entry dialog for GnuPG
ii  pinentry-gtk2     1.1.0-1   amd64   GTK+-2-based PIN or pass-phrase entry dialog for GnuPG
ii  pinentry-tty      1.1.0-1   amd64   minimal dumb-terminal PIN or pass-phrase entry for GnuPG
```

```
$ echo $GPG_AGENT_INFO
/run/user/1000/gnupg/S.gpg-agent:0:1
```

```
taha@asks2:~/.local/share/applications
(i3) $ cat rofi-pass.desktop
[Desktop Entry]
Name=pass
Comment=Script to handle Password Store using rofi
GenericName=rofi-pass
Exec=/home/taha/.local/bin/rofi-pass
Type=Application
Terminal=false
```

```
taha@asks2:~
$ ll .local/bin/rofi-pass
lrwxrwxrwx 1 taha taha 41 Jun 29  2019 .local/bin/rofi-pass -> /home/taha/.local/git/rofi-pass/rofi-pass
```

```
$ ll .config/rofi-pass/
total 8.0K
drwxrwxr-x  2 taha taha 4.0K Jun 29  2019 .
drwxr-xr-x 83 taha taha 4.0K May 31 14:41 ..
lrwxrwxrwx  1 taha taha   38 Jun 29  2019 config -> /home/taha/.local/git/rofi-pass/config
```

```
taha@asks2:~
$ gpg --version
gpg (GnuPG) 2.2.4
libgcrypt 1.8.1
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /home/taha/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
taha@asks2:~
$ gpg-agent --version
gpg-agent (GnuPG) 2.2.4
libgcrypt 1.8.1
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

```
taha@asks2:~
$ gpg-agent --help
gpg-agent (GnuPG) 2.2.4
libgcrypt 1.8.1
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Syntax: gpg-agent [options] [command [args]]
Secret key management for GnuPG

Options:

     --daemon                        run in daemon mode (background)
     --server                        run in server mode (foreground)
     --supervised                    run in supervised mode
 -v, --verbose                       verbose
 -q, --quiet                         be somewhat more quiet
 -s, --sh                            sh-style command output
 -c, --csh                           csh-style command output
     --options FILE                  read options from FILE
     --no-detach                     do not detach from the console
     --log-file                      use a log file for the server
     --pinentry-program PGM          use PGM as the PIN-Entry program
     --scdaemon-program PGM          use PGM as the SCdaemon program
     --disable-scdaemon              do not use the SCdaemon
     --extra-socket NAME             accept some commands via NAME
     --keep-tty                      ignore requests to change the TTY
     --keep-display                  ignore requests to change the X display
     --default-cache-ttl N           expire cached PINs after N seconds
     --ignore-cache-for-signing      do not use the PIN cache when signing
     --no-allow-external-cache       disallow the use of an external password cache
     --no-allow-mark-trusted         disallow clients to mark keys as "trusted"
     --allow-preset-passphrase       allow presetting passphrase
     --no-allow-loopback-pinentry    disallow caller to override the pinentry
     --allow-emacs-pinentry          allow passphrase to be prompted through Emacs
     --enable-ssh-support            enable ssh support
     --ssh-fingerprint-digest ALGO   use ALGO to show ssh fingerprints

Please report bugs to <https://bugs.gnupg.org>.
```

Note the `--default-cache-ttl` option.

https://unix.stackexchange.com/questions/145233/how-do-i-make-gpg-agent-forget-my-passphrase-automatically


```
taha@asks2:~
$ gpgconf
gpg:OpenPGP:/usr/bin/gpg
gpg-agent:Private Keys:/usr/bin/gpg-agent
scdaemon:Smartcards:/usr/lib/gnupg/scdaemon
gpgsm:S/MIME:/usr/bin/gpgsm
dirmngr:Network:/usr/bin/dirmngr
pinentry:Passphrase Entry:/usr/bin/pinentry
```

So I should just create `~/.gnupg/gpg-agent.conf`:

```
default-cache-ttl 1800 # default is 600
max-cache-ttl 7200 # default is 7200
```

+ https://rutar.org/writing/some-tricks-with-unix-pass/
+ https://unix.stackexchange.com/questions/46960/how-to-configure-gpg-to-enter-passphrase-only-once-per-session
+ https://unix.stackexchange.com/questions/303298/make-gpg-agent-permanently-store-passphrase
+ https://unix.stackexchange.com/questions/608715/how-to-get-passphrase-cache-duration-left-in-gpg-agent
+ https://unix.stackexchange.com/questions/624918/starting-gpg-agent-with-specific-options
+ https://unix.stackexchange.com/questions/145233/how-do-i-make-gpg-agent-forget-my-passphrase-automatically
+ http://manpages.ubuntu.com/manpages//artful/man1/gpg-agent.1.html


> Below are my original notes (note that many of them pre-date the code in this role).


## Import your private GPG key

I've not been able to make the existing procedure work.
Perhaps the slightly newer GPG version on Ubuntu 18.04 is different?
I was able to confirm that the issue with importing the public-private symmetrically
encrypted file had nothing to do with Ansible, rather it was GPG itself that
behaved in an unexpected way.

NO, STRIKE THE ABOVE. Perhaps we are simply missing some pinentry or dirmngr dependency?

For reference, this *works* on `x230t`:
```
$ gpg --decrypt --output - <gpgkeyid>.asc | gpg --import
```
(note that `--decrypt` is the default command and assumed by gpg if no other command is given).

This requires us to get around Ansible's inability to handle the interactive GPG passphrase prompts.

### Using expect

Import GPG key:
```
expect -c "spawn gpg --decrypt --output armant-gitea armant-gitea.gpg; send "
```

Trust GPG key:
```
gpg --edit-key {KEY} trust quit
# enter 5<RETURN>
# enter y<RETURN>
```

Equivalent command using `expect`:
```
expect -c "spawn gpg --edit-key {KEY} trust quit; send \"5\ry\r\"; expect eof"
```

+ https://askubuntu.com/questions/338857/automatically-enter-input-in-command-line/338860#338860
+ https://docs.ansible.com/ansible/latest/modules/expect_module.html
+ https://unix.stackexchange.com/questions/184947/how-to-import-secret-gpg-key-copied-from-one-machine-to-another


### What if we take a copy of the entire ~/.gnupg folder?

Which files are actually necessary to copy over?

```
taha@asks2:~/nextcloud/chepec/briefcase/keys/gpg
$ cp ~/.gnupg/*.gpg .
taha@asks2:~/nextcloud/chepec/briefcase/keys/gpg
$ ll
total 20K
drwxr-xr-x 2 taha taha 4.0K Jun  7 03:41 .
drwxr-xr-x 4 taha taha 4.0K Jun  6 21:17 ..
-rw------- 1 taha taha 1.2K Jun  7 03:41 pubring.gpg
-rw------- 1 taha taha 2.6K Jun  7 03:41 secring.gpg
-rw------- 1 taha taha 1.3K Jun  7 03:41 trustdb.gpg
```

+ https://gist.github.com/chrisroos/1205934
+ https://www.phildev.net/pgp/gpg_moving_keys.html

Turns out, that even so, we still need to supply the passphrase when decrypting something.
Let's try to use something like this, so that gpg does not open the interactive prompt:
+ https://unix.stackexchange.com/a/415064
+ https://superuser.com/a/1280848

```
echo "<passphrase>" | gpg --pinentry-mode loopback --passhprase-fd 0 armant-gitea.gpg
```

Perhaps we could even combine the above with Ansible's `vars_prompt` feature so
that the passphrase need not be stored at all in the playbook?

Note: this looks like it will work for the gitea key, but does not appear to
properly setup/configure the user's GPG AGENT on the system.

+ https://unix.stackexchange.com/questions/303298/make-gpg-agent-permanently-store-passphrase


### Ansible Vault's effect on filesize

```
$ ll roles/dotssh/files/
total 20K
drwxr-xr-x 2 taha taha 4.0K Jun  7 04:13 .
drwxr-xr-x 6 taha taha 4.0K Jun  7 04:13 ..
-rw------- 1 taha taha 1.2K Jun  7 03:41 pubring.gpg
-rw------- 1 taha taha 2.6K Jun  7 03:41 secring.gpg
-rw------- 1 taha taha 1.3K Jun  7 03:41 trustdb.gpg
$ ansible-vault encrypt roles/dotssh/files/pubring.gpg
New Vault password:
Confirm New Vault password:
Encryption successful
$ ll roles/dotssh/files/
total 24K
drwxr-xr-x 2 taha taha 4.0K Jun  7 04:14 .
drwxr-xr-x 6 taha taha 4.0K Jun  7 04:13 ..
-rw------- 1 taha taha 5.1K Jun  7 04:14 pubring.gpg
-rw------- 1 taha taha 2.6K Jun  7 03:41 secring.gpg
-rw------- 1 taha taha 1.3K Jun  7 03:41 trustdb.gpg
```




## Get the passwordstore

Start by getting my GPG key onto this system.
On a system with the key,
```
gpg -K
```

Now export the public key. This is of course public info, so no worries about security here
```
gpg --output pubkey.gpg --export "<gpgkeyid>"
```

Now, in one fluid step, we will export the secret key, combine it into one file
with the public key, and then encrypt it for transfer
```
gpg --output - --export-secret-key "<gpgkeyid>" | cat pubkey.gpg - | gpg --armor --output keys.asc --symmetric --cipher-algo AES256
```

You will be prompted for a passphrase during this, this is the passphrase just
for this temporary encryption for transfer.
So use a good passphrase for security, and remember that passphrase!
Now, transfer the `keys.asc` file to the target computer.

On the target computer, we need to import our key from this encrypted file.
```
gpg --no-use-agent --output - keys.asc | gpg --import
```

This threw an error:
`gpg: key <keyid>: error sending to agent: Inappropriate ioctl for device`,
causing key import to fail.
https://d.sb/2016/11/gpg-inappropriate-ioctl-for-device-errors

> To solve the problem, you need to enable `loopback` pinentry mode.
> Add this to `~/.gnupg/gpg.conf`:
> ```
> use-agent
> pinentry-mode loopback
> ```
> And add this to `~/.gnupg/gpg-agent.conf`, creating the file if it doesn't already exist:
> `allow-loopback-pinentry`
> Then restart the agent with `echo RELOADAGENT | gpg-connect-agent` and
> you should be good to go.

Now it indeed works! (NOTE: you need to revert these changes otherwise regular
`pass` usage won't work).
```
$ gpg --no-use-agent --output - keys.asc | gpg --import
gpg: WARNING: "--no-use-agent" is an obsolete option - it has no effect
gpg: WARNING: no command supplied.  Trying to guess what you mean ...
gpg: AES256 encrypted data
gpg: encrypted with 1 passphrase
gpg: key C34943683467923B: "Taha Ahmed <gpgkeyid>" not changed
gpg: key C34943683467923B: "Taha Ahmed <gpgkeyid>" not changed
gpg: key C34943683467923B: secret key imported
gpg: Total number processed: 2
gpg:              unchanged: 2
gpg:       secret keys read: 1
gpg:   secret keys imported: 1
```

Lastly, you might want to trust the key "ultimately" for smoother use.
```
gpg --edit-key "<gpgkeyid>"
gpg> trust
# select "ultimate trust"
```

Now we can get the password store itself.
```
$ sudo apt install pass
$ pass init "<gpgkeyid>"
$ rm .password-store/.gpg-id
$ git clone <pwstore-gitrepo> ~/.password-store
```
